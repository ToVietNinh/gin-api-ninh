package middlewares

import (
	"context"
	"fmt"
	"gin-api/models"

	// "gin-api/controllers"
	// "gin-api/controllers"

	"log"
	"net/http"
	"os"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type AuthMiddleware struct {
	usercollection *mongo.Collection
	ctx            context.Context
	model          *models.User
}

func NewAuthMiddleware(usercollection *mongo.Collection) AuthMiddleware {
	return AuthMiddleware{
		usercollection: usercollection,
	}
}
func (aw *AuthMiddleware) VerifyToken(usercollection *mongo.Collection) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		err1 := godotenv.Load()
		if err1 != nil {
			panic(err1)
		}
		h := models.Header{}
		err := ctx.ShouldBindHeader(&h)
		if err != nil {
			ctx.JSON(http.StatusOK, gin.H{
				"message": err.Error(),
			})
		}
		log.Println("AAAAAAAAAAAAAA")
		log.Println(h)
		if len(h.Authorization) == 0 {
			ctx.JSON(http.StatusBadRequest, gin.H{
				"message": "NO TOKEN || Login to access this route",
			})
			ctx.Abort()
		} else {
			tknStr := strings.Split(h.Authorization, " ")[1]
			jwtToken := &models.JwtToken{}

			key := []byte(os.Getenv("JWT_KEY"))

			token, err := jwt.ParseWithClaims(tknStr, jwtToken, func(token *jwt.Token) (interface{}, error) {
				return key, nil
			})
			log.Println("++++++++++++")
			log.Println(jwtToken)

			if err != nil {
				if err == jwt.ErrSignatureInvalid {
					ctx.JSON(http.StatusBadRequest, gin.H{
						"message": "Token Invalid, Login to access this route",
					})
					ctx.Abort()
				}
			}

			if !token.Valid {
				ctx.JSON(http.StatusBadRequest, gin.H{
					"message": "Token Invalid, Login to access this route",
				})
				// mqtt : giao thuc facebook
				ctx.Abort()
			}
			query := bson.D{bson.E{Key: "_id", Value: jwtToken.ID}}
			// query := bson.M{"username": jwtToken.Username}
			fmt.Println(query)
			var userOuput *models.User
			err2 := usercollection.FindOne(ctx, query).Decode(&userOuput)
			// cursor, err2 := usercollection.Find(ctx, bson.D{})
			fmt.Println("-------+++++++")
			fmt.Println(userOuput)

			if err2 != nil {
				ctx.JSON(http.StatusBadRequest, gin.H{
					"message": "User can not access!",
				})
				ctx.Abort()
			}

			ctx.JSON(http.StatusOK, gin.H{
				"ID":       userOuput.ID,
				"username": userOuput.Username,
			})
			ctx.Next()
		}

	}
}
