package main

import (
	"context"
	"fmt"
	"gin-api/controllers"
	"gin-api/middlewares"
	"log"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var (
	mongoclient *mongo.Client
	err         error
	ctx         context.Context
	tc          controllers.TaskController
	uc          controllers.UserController
	ad          middlewares.AuthMiddleware
	taskc       *mongo.Collection
	userc       *mongo.Collection
)

func main() {
	router := gin.Default()
	mongoconn := options.Client().ApplyURI("mongodb://localhost:27017")
	mongoclient, err = mongo.Connect(ctx, mongoconn)
	if err != nil {
		log.Fatal("error while connecting with mongo", err)
	}
	err = mongoclient.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal("error while trying to ping mongo", err)
	}

	fmt.Println("mongo connection established")
	taskc = mongoclient.Database("taskdb").Collection("tasks")
	tc = controllers.NewTask(taskc, ctx)
	ad = middlewares.NewAuthMiddleware(userc)
	userc = mongoclient.Database("taskdb").Collection("users")
	uc = controllers.NewUser(userc, ctx)

	defer mongoclient.Disconnect(ctx)
	routerBase := router.Group("/api")
	uc.RegisterAuthenRoutes(routerBase)
	routerBase.Use(ad.VerifyToken(userc))
	tc.RegisterTaskRoutes(routerBase)
	router.Run(":8080")
}
