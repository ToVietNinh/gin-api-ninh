package models

import (
	"github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type User struct {
	ID primitive.ObjectID `bson:"_id"`
	// First_name string `json:"first_name" validate:"required, min=2, max=100"`
	// Last_name  string `json:"last_name" validate:"required, min=2, max=100"`
	Username string `bson:"username" validate:"required"`
	Password string `bson:"password" validate:"required, min=6"`
	// Email         string    `json:"email" validate:"email, required"`
	// Phone         string    `json:"phone" validate:"required"`
	// Token         string    `json:"token"`
	// Refresh_token string    `json:"refresh_token"`
	// Created_at    time.Time `json:"created_at"`
	// Updated_at    time.Time `json:"updated_at"`
	// User_type     string    `json:"user_type"`
	// User_id primitive.ObjectID `json:"user_id omitempty"`
}

type JwtToken struct {
	ID       primitive.ObjectID `bson:"_id"`
	Username string             `bson:"username"`
	jwt.StandardClaims
}

type Header struct {
	Authorization string `header:"Authorization"`
}

// var (
// 	usercollection *mongo.Collection
// )

// func (u *User) SaveUser(ctx context.Context) (*User, error) {
// 	var err error
// 	_, err = usercollection.InsertOne(ctx, &u)
// 	if err != nil {
// 		return &User{}, err
// 	}
// 	return u, nil
// }
