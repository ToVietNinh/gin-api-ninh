package controllers

import (
	"context"
	"fmt"
	"gin-api/models"
	"log"
	"os"
	"time"

	// "log"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/crypto/bcrypt"
)

type UserController struct {
	usercollection *mongo.Collection
	ctx            context.Context
	model          *models.User
	// authmiddleware *middlewares.AuthMiddleware
}

func NewUser(usercollection *mongo.Collection, ctx context.Context) UserController {
	return UserController{
		usercollection: usercollection,
		ctx:            ctx,
		// authmiddleware: authmiddleware,
	}
}

func generateHashPassword(pwd string) string {
	BBpwd := []byte(pwd)
	hash, err := bcrypt.GenerateFromPassword(BBpwd, 15)
	if err != nil {
		log.Fatal(err)
	}
	return string(hash)
}

func compareHashPassword(hash string, pwd string) bool {
	BBhash := []byte(hash)
	BBpwd := []byte(pwd)
	err := bcrypt.CompareHashAndPassword(BBhash, BBpwd)
	if err != nil {
		return false
	}
	return true
}

// Register

type RegisterInput struct {
	Username string `bson:"username" binding:"required"`
	Password string `bson:"password" binding:"required"`
}

func (uc *UserController) postRegister(ctx *gin.Context) {
	var userInput *RegisterInput
	// username := ctx.PostForm("username")
	// password := ctx.PostForm("password")
	if err := ctx.ShouldBindJSON(&userInput); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}
	fmt.Println("__________________")
	fmt.Println(userInput)
	userInput.Password = generateHashPassword(userInput.Password)

	_, err := uc.usercollection.InsertOne(ctx, userInput)

	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"username": userInput.Username,
		"password": userInput.Password,
	})
}

// Login

type LoginInput struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

// func (uc *UserController) generateToken() string {

// }

func (uc *UserController) postLogin(ctx *gin.Context) {
	error := godotenv.Load()
	if error != nil {
		panic(error)
	}
	log.Println(os.Getenv("JWT_KEY"))
	var userLogin *models.User
	if err := ctx.ShouldBindJSON(&userLogin); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})
		return
	}
	log.Println(userLogin)
	filter := bson.M{"username": userLogin.Username}
	result := models.User{}

	err := uc.usercollection.FindOne(ctx, filter).Decode(&result)
	cResult := compareHashPassword(result.Password, userLogin.Password)
	log.Println(cResult)
	log.Println("asdsadasdasdsadsa1-----")
	if err != nil {
		ctx.JSON(200, gin.H{
			"message": "User not Found!",
		})
		return
	}
	if !cResult {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "Wrong password!",
		})
		return
	}
	expiration := time.Now().Add(100 * time.Minute)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &models.JwtToken{
		ID:       result.ID,
		Username: result.Username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expiration.Unix(),
		},
	})
	// var keyPair *KeyPair
	tokenString, err := token.SignedString([]byte(os.Getenv("JWT_KEY")))
	log.Println(token)
	log.Println(tokenString)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "Internal Server Error",
		})
		return
	}
	refreshToken := jwt.NewWithClaims(jwt.SigningMethodHS256, &models.JwtToken{
		ID:       result.ID,
		Username: result.Username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expiration.Unix(),
		},
	})
	refreshTokenString, err := refreshToken.SignedString([]byte(os.Getenv("JWT_KEY")))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"message": "Internal Server Error",
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"user":            result.Username,
		"token":           tokenString,
		"refreshToken":    refreshTokenString,
		"tokenExpiration": 1,
	})
}

func (uc *UserController) RegisterAuthenRoutes(rg *gin.RouterGroup) {
	userroute := rg.Group("/")
	userroute.POST("/register", uc.postRegister)
	userroute.POST("/login", uc.postLogin)
}
