package controllers

import (
	"context"
	"gin-api/models"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type TaskController struct {
	taskcollection *mongo.Collection
	ctx            context.Context
	model          *models.Task
}

func NewTask(taskcollection *mongo.Collection, ctx context.Context) TaskController {
	return TaskController{
		taskcollection: taskcollection,
		ctx:            ctx,
	}
}

func (tc *TaskController) getAll(ctx *gin.Context) {
	var tasks []*models.Task
	title := ctx.DefaultQuery("title", "No title")
	description := ctx.Query("description")
	log.Println(title, description)
	cursor, err := tc.taskcollection.Find(ctx, bson.D{{}})
	if err != nil {
		panic(err)
	}
	// log.Printf("%s", cursor)
	for cursor.Next(tc.ctx) {
		var task models.Task
		err := cursor.Decode(&task)
		if err != nil {
			panic(err)
		}
		tasks = append(tasks, &task)
	}
	if err := cursor.Err(); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
	}
	cursor.Close(tc.ctx)
	if len(tasks) == 0 {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, tasks)
}

func (tc *TaskController) getTask(ctx *gin.Context) {
	var titleName string = ctx.Param("title")
	var task *models.Task
	query := bson.D{bson.E{Key: "title", Value: titleName}}
	err := tc.taskcollection.FindOne(tc.ctx, query).Decode(&task)
	log.Println(task)
	log.Println("***********")
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, task)
}

func (tc *TaskController) createTask(ctx *gin.Context) {
	var task models.Task
	if err := ctx.ShouldBindJSON(&task); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
		return
	}
	_, err := tc.taskcollection.InsertOne(tc.ctx, task)
	if err != nil {
		ctx.JSON(http.StatusBadGateway, gin.H{"message": err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Successfully!"})
}

func (tc *TaskController) updateTask(ctx *gin.Context) {
	var task models.Task
	if err := ctx.ShouldBindJSON(&task); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
	}
	filter := bson.D{primitive.E{Key: "title", Value: task.Title}}
	update := bson.D{primitive.E{Key: "$set", Value: bson.D{primitive.E{Key: "title", Value: task.Title}, primitive.E{Key: "description", Value: task.Description}, primitive.E{Key: "statusdone", Value: task.StatusDone}}}}
	result, _ := tc.taskcollection.UpdateOne(tc.ctx, filter, update)
	if result.MatchedCount != 1 {
		ctx.JSON(http.StatusBadGateway, gin.H{"message": "no matched document found for update"})
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Successfully!"})
}

func (tc *TaskController) DeleteTask(ctx *gin.Context) {
	var title string = ctx.Param("title")
	filter := bson.D{primitive.E{Key: "title", Value: title}}
	result, _ := tc.taskcollection.DeleteOne(tc.ctx, filter)
	if result.DeletedCount != 1 {
		ctx.JSON(http.StatusBadGateway, gin.H{"message": "no matched document found for delete"})
	}
	ctx.JSON(http.StatusOK, gin.H{"message": "Successfully!"})
}

func (tc *TaskController) RegisterTaskRoutes(rg *gin.RouterGroup) {
	taskroute := rg.Group("/task")
	taskroute.GET("/test", func(ctx *gin.Context) {
		ctx.JSON(200, gin.H{
			"message": "test successful",
		})
	})
	// taskroute.Use(mdw.VerifyToken())
	// {
	taskroute.GET("/getAll", tc.getAll)
	taskroute.GET("/get/:title", tc.getTask)
	taskroute.POST("/create", tc.createTask)
	taskroute.PATCH("/update", tc.updateTask)
	taskroute.DELETE("/delete/:title", tc.DeleteTask)
	// }

}
